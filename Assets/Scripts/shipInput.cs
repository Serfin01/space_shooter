﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipInput : MonoBehaviour
{
     public Vector2 axis;
     
    public PlayerBehaviour player;

    // Update is called once per frame
    void Update()
    {
        axis.x = Input.GetAxis ("Horizontal");
        axis.y = Input.GetAxis ("Vertical");
        //Debug.Log("x:"+axis.x+"y:"+axis.y);
        if (Input.GetButton("Fire1"))
        {
            player.Shoot();
        }
        if (Input.GetButton("Fire2"))
        {
            player.Shoot();
        }
        if (Input.GetButton("Fire3"))
        {
            player.Shoot();
        }
        if (Input.GetButton("Jump"))
        {
            player.Shoot();
        }

        player.SetAxis(axis);
    }
}
