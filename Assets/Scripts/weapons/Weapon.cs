﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon: MonoBehaviour
{
    public abstract float GetCadencia();

    public abstract void Shoot();

    public abstract void Shoot2();

    public abstract void Shoot3();

    public abstract void Shoot4();


}
