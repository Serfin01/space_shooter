﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserWeapon : Weapon
{

    public GameObject laserBullet;
    public GameObject abanicoBullet;
    public float cadencia;
    public AudioSource audioSource;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
        audioSource.Play();
    }
    public override void Shoot2()
    {
        Instantiate(abanicoBullet, this.transform.position, Quaternion.identity, null);

    }
    public override void Shoot3()
    {
        Instantiate(abanicoBullet, this.transform.position, Quaternion.identity, null);

    }
    public override void Shoot4()
    {
        Instantiate(abanicoBullet, this.transform.position, Quaternion.identity, null);

    }
}
