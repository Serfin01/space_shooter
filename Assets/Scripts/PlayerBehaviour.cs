﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
    public GameObject ScoreManager;

    public Weapon weapon;

    public float shootTime = 0;

    public Propeller prop;

    public GameObject graphics;
    public AudioSource audioSource;
    [SerializeField] Collider2D collider;

    public ParticleSystem ps;

    private bool iamDead = false;

    public int lives = 3;

    public void SetAxis (Vector2 currentAxis){
        axis = currentAxis;
    }
    public void Shoot(){
        //Debug.Log("Dispara");
        if (shootTime > weapon.GetCadencia())
            {
                shootTime = 0f;
                weapon.Shoot();
        }
        }
    

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Meteor")
        {
            StartCoroutine(DestroyShip());
        }
    }
    
    IEnumerator DestroyShip()
    {
        //Indico que estoy muerto
        iamDead = true;

        //Me quito una vida
        lives--;

        //Desactivo el grafico
        graphics.SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Desactivo el propeller
        prop.gameObject.SetActive(false);

        //decimos al score que hemos muerto
        ScoreManager.LoseLife();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Miro si tengo mas vidas
        if (lives > 0)
        {
            //Vuelvo a activar el jugador
            iamDead = false;
            graphics.SetActive(true);
            //Activo el propeller
            
            StartCoroutine(inMortal());
        }
        
        prop.gameObject.SetActive(true);

        
    }
    IEnumerator inMortal()
    {
        iamDead = false;
        graphics.SetActive(true);
        prop.gameObject.SetActive(true);

        for(int i = 0; i < 15; i++)
        {
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        collider.enabled = true;
    }


    void Update (){
        if (iamDead){
            return;
        }
        shootTime += Time.deltaTime;
        transform.Translate (axis * speed * Time.deltaTime);
        if (transform.position.y > limits.y)
        {
            transform.position = new Vector3(transform.position.x, limits.y*-1, transform.position.z);
        }
        else if (transform.position.y*-1 > limits.y)
        {
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
        }
        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
        }
        else if (transform.position.x*-1 > limits.x)
        {
            transform.position = new Vector3(limits.x*-1, transform.position.y, transform.position.z);
        }
        if(axis.x>0){
            prop.BlueFire();
        }else if(axis.x<0){
            prop.RedFire();
        }else{
            prop.StopBlue();
            prop.StopRed();
        }
        
        

    }
}
