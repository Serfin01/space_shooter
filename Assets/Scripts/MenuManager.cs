﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay(){
        Debug.LogError("He pulsado Play");
        SceneManager.LoadScene("Game");
    }
    public void PulsaCredits(){
        Debug.LogError("He pulsado Creditos");
        
    }
    public void PulsaExit(){
        Debug.LogError("He pulsado Exit");
        Application.Quit();
    }
}
